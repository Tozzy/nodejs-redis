// index.js --> MAIN DE MON APPLICATION COTER SERVER 

// --> appRoutes == utile que pour mongoDB 
// --> dataSchema == utile que pour mongoDB

// --> Redis la partie qui nous intéresse est ci-dessous 

// Redis-web --> est le FRONT générer avec angular pas complet juste générer

/*
var http = require('http');

var port = process.env.PORT || 8080;

var appRoutes = require('./routes/appRoutes');


// ESSAI CONNEXION MONGO DB (ECHEC)

var mongoose = require('mongoose');

mongoose.connect('mongodb:/localhost:27017', {useMongoClient:true});
app.use('/', appRoutes);

http.createServer(app).listen(port);

console.log("Backend running on port : ", port);
*/

var express = require('express');
var cors = require('cors');
var app = express(); // create express app

app.use(cors());

var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());




 // ESSAI CONNEXION REDIS (SUCCES)

const redis = require('redis');
const client = redis.createClient(); // Connexion redis en local.

// Controle erreurs redis sur la console
client.on('error', (err) => {
    console.log("Error " + err)
});


app.get('/employer', (req, res) => {
    const todoRedisKey = 'employer';
    client.get(todoRedisKey, (err, employer) => {
        // Si la clé redis existe
        if (employer) {
            console.log('Lecture des donnée employer-----');
            res.json(JSON.parse(employer))
        } 
    });
});

app.get('/configTerminal', (req, res) => {
    const todoRedisKey = 'configTerminal';
    client.get(todoRedisKey, (err, configTerminal) => {
        // Si la clé redis existe
        if (configTerminal) {
            console.log('Lecture des donnée configTerminal-----');
            res.json(JSON.parse(configTerminal))
        } 
    });
});

/*
app.all('/employer', function (req, res, next) {
    console.log('Toute demmande HTTP possible ...');
    next(); // pass control to the next handler
  });
*/

  // Démarage du serveur express sur le port 3000
app.listen(3000, () => {
    console.log('Server démarer sur le port: ', 3000)
});

 