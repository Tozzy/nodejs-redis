const mongoose = require('mongoose');

// CECI EST UN SCHEMA PREDEFINIE POUR MONGO DB

var employerSchema = mongoose.Schema({
  nom: { type:String },
  prenom:{type:String},
  age:{type:String},
  salaire:{type:String}
});

module.exports=mongoose.model('employer', employerSchema);