
// CECI EST POUR MANGO DB C'EST LA GESTION DES ROUTES

var express = require('express');
var router = express.Router();
var Employer = require('../models/dataSchema');

router.post('/create', (req, res, next) => {
  var newEmployer = new Employer({
    nom : req.body.nom,
    prenom : req.body.prenom,
    age : req.body.age,
    salaire : req.body.salaire
  });
  newEmployer.save((err, employer) => {
    if(err)
      res.status(500).json({errmsg : err});
    res.status(200).json({msg : employer});
  })
  
});

router.get('/read', (req, res, next) => {
  Employer.find({}, (err, employer) => {
    if(err)
      res.status(500).json({errmsg: err});
    res.status(200).json({msg : employer});
  })
  
});

router.put('/update', (req, res, next) => {
  Employer.findById(req.body._id, (err, employer) => {
    if(err)
      res.status(500).json({errmsg: err});
    employer.nom = req.body.nom;
    employer.prenom = req.body.prenom;
    employer.age = req.body.age;
    employer.salaire = req.body.salaire;
    employer.save((err, employer) => {
      if(err)
        res.status(500).json({errmsg: err});
      res.status(200).json({msg: employer});
    }) 
  })
});

router.delete('/delete/:id', (req, res, next) => {
  Employer.findOneAndRemove({_id:req.params._id}, (err, employer) => {
    if(err)
        res.status(500).json({errmsg: err});
      res.status(200).json({msg: employer});
  })
});

module.exports = router;